Tanane Web Application
=========================

Pasos a seguir para el despliegue de la aplicación
=========================

- Desde la raíz del directorio web donde esta el proyecto ejecutar el comando `git pull`, si el comando retorno algun error entonces ejecutar primero `git reset --hard HEAD` para apuntar la cabecera del repositorio al HEAD del master
- Eliminar los contenidos de las carpetas `/app/cache`, `/app/logs`, `/web/css` y `/web/js` (`rm -rf /path/to/directory`)
- Desde la raíz del directorio web donde esta el proyecto y en la consola de *nix/Windows/OsX ejecutar el comando `composer update` para actualizar los `vendor` del proyecto
- Desde la raíz del directorio web donde esta el proyecto y en la consola de *nix/Windows/OsX ejecutar el comando `php app/console --shell` para abrir el shell interactivo de Symfony2
- Verificamos si la BD y el mapeado de entidades desde Symfony2 estan en correspondencia ejecutando el comando `doctrine:schema:validate` si dio algun error registrarlo en los [issues][1] del proyecto
- Ejecutamos el comando `doctrine:migrations:diff` para generar la migración entre la BD y el mapeado actual (así se mantiene un versionado de la BD y se puede devolver en cualquier momento a una versión anterior)
- Ejecutamos el comando `doctrine:migrations:migrate` para actualizar el modelo de datos físico
- Ejecutamos el comando `sp:bower:install` si no se ha ejecutado previamente o `sp:bower:update` para actualizar las librerias de Javascript usadas en el proyecto y manejadas mediante Bower, una especie de Composer pero para Javascript
- Ejecutamos el comando `assets:install --symlink` para crear los enlaces simbólicos a las carpetas de recursos de cada uno de los Bundles según la configuración establecida en `config.yml`
- Ejecutamos los comandos `assetic:dump` y `assetic:dump --env=prod` para hacer un despliegue de los elementos web del proyecto y enmascarar las rutas hacia los mismos de esta forma nadie sabrá donde están ubicados dichos archivos ni a cuales Bundles pertenecen
- Probamos que todo funcione correctamente

 [1]: https://bitbucket.org/iosev/sis-php-source/issues/new
