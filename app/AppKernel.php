<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{

    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
            new Genemu\Bundle\FormBundle\GenemuFormBundle(),
            new Mremi\ContactBundle\MremiContactBundle(),
            new Hype\MailchimpBundle\HypeMailchimpBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle(),
            new Misd\PhoneNumberBundle\MisdPhoneNumberBundle(),
            new RaulFraile\Bundle\LadybugBundle\RaulFraileLadybugBundle(),
            new Sp\BowerBundle\SpBowerBundle(),
            new WhiteOctober\TCPDFBundle\WhiteOctoberTCPDFBundle(),
            # Tanane Bundles and dependencies
            new Tanane\CommonBundle\CommonBundle(),
            new Tanane\FrontendBundle\FrontendBundle(),
            new Tanane\ProductBundle\ProductBundle(),
            new Tanane\BackendBundle\BackendBundle(),
            new Tanane\UserBundle\UserBundle(),
            new Tanane\TemplateBundle\TemplateBundle(),
        );

        if (in_array($this->getEnvironment(), array( 'dev', 'test' )))
        {
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
            $bundles[] = new h4cc\AliceFixturesBundle\h4ccAliceFixturesBundle();
        }

        return $bundles;

    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__ . '/config/config_' . $this->getEnvironment() . '.yml');

    }

}
