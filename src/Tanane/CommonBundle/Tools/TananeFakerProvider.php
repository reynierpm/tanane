<?php
/**
 * RolesFakerProvider
 *
 * @package     Tanane
 * @subpackage  CommonBundle
 * @author      Reynier Perez Mira <reynierpm@gmail.com>
 * @copyright   (c) Dynamo Technology Solutions
 */

namespace Tanane\CommonBundle\Tools;

use Symfony\Component\DependencyInjection\Container;

class TananeFakerProvider
{
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function randomRoles()
    {
        $names = ['ROLE_USER', 'ROLE_PROFILE_ONE', 'ROLE_PROFILE_TWO'];

        return [$names[array_rand($names)]];
    }

    public function formatPhoneNumber($fakePhoneNumber)
    {
        return $this->container->get('libphonenumber.phone_number_util')->parse($fakePhoneNumber, "VE");
    }
}
