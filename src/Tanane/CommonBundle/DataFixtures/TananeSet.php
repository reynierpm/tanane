<?php
$set_config = array(
    'locale' => 'es_ES',
    'seed' => 42,
    'do_drop' => true,
    'do_persist' => true,
);

$set = new h4cc\AliceFixturesBundle\Fixtures\FixtureSet($set_config);

$set->addFile(__DIR__.'/Bank.yml', 'yaml');
$set->addFile(__DIR__.'/OrderStatus.yml', 'yaml');
$set->addFile(__DIR__.'/PaymentType.yml', 'yaml');
$set->addFile(__DIR__.'/Group.yml', 'yaml');
$set->addFile(__DIR__.'/User.yml', 'yaml');
$set->addFile(__DIR__.'/Product.yml', 'yaml');
$set->addFile(__DIR__.'/Person.yml', 'yaml');
$set->addFile(__DIR__.'/Natural.yml', 'yaml');
$set->addFile(__DIR__.'/Legal.yml', 'yaml');
$set->addFile(__DIR__.'/Orders.yml', 'yaml');
$set->addFile(__DIR__.'/OrderHasProduct.yml', 'yaml');

return $set;
