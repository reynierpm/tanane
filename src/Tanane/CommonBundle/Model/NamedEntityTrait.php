<?php
/**
 * Trait:       Base para entidades que usen el atributo name
 *
 * @package     Tanane
 * @subpackage  CommonBundle
 * @author      Reynier Perez Mira <reynierpm@gmail.com>
 * @copyright   (c) Dynamo Technology Solutions
 */

namespace Tanane\CommonBundle\Model;

use Doctrine\ORM\Mapping as ORM;

trait NamedEntityTrait
{
    /**
     * @ORM\Column(type="string",nullable=false,length=50)
     */
    protected $name;

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }
}
