<?php
/**
 * Trait:       Entidades con Identificador
 *
 * @package     Tanane
 * @subpackage  CommonBundle
 * @author      Reynier Perez Mira <reynierpm@gmail.com>
 * @copyright   (c) Dynamo Technology Solutions
 */

namespace Wuelto\Common\CommonBundle\Model;

use Doctrine\ORM\Mapping as ORM;

trait IdentifiedEntityTrait
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer",unique=true,nullable=false)
     */
    protected $id;

    public function getId()
    {
        return $this->id;
    }
}
