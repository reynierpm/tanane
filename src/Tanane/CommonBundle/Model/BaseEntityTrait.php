<?php
/**
 * Trait:       Base para entidades que usen Timestampable y Softdeleteable
 *
 * @package     Tanane
 * @subpackage  CommonBundle
 * @author      Reynier Perez Mira <reynierpm@gmail.com>
 * @copyright   (c) Dynamo Technology Solutions
 */

namespace Tanane\CommonBundle\Model;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

trait BaseEntityTrait
{
    /**
     * @ORM\Column(name="created", type="datetime",nullable=false)
     * @Gedmo\Timestampable(on="create")
     */
    protected $created;

    /**
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="update")
     */
    protected $updated;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    protected $deletedAt;

    public function setCreated($created)
    {
        $this->created = $created;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }

    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
