$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();

    $('select').select2({
        placeholder: "Seleccione una opción"
    });

    $('.img').on({
        'mouseover': function() {
            var img = '/bundles/frontend/images/' + $(this).attr('id') + '-1.jpg';
            $(this).attr('src', img);
        },
        mouseout: function() {
            var img = '/bundles/frontend/images/' + $(this).attr('id') + '.jpg';
            $(this).attr('src', img);
        }
    });
});