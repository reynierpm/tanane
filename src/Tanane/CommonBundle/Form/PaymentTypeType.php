<?php

namespace Tanane\CommonBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PaymentTypeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('name', 'text', array(
                    'required' => true,
                    'label' => "Tipo de Pago",
                    'trim' => true,
                ))
                ->add('active', null, array(
                    'required' => FALSE,
        ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Tanane\CommonBundle\Entity\PaymentType',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'paymenttype';
    }
}
