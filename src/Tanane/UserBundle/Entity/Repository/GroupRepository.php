<?php

namespace Tanane\UserBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * GroupRepository Class
 *
 */
class GroupRepository extends EntityRepository
{
    /**
     * Check if group is assigned to any user
     *
     * @param  int   $id The group ID
     * @return array The objects.
     */
    public function groupIsAssigned($id)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb
                ->select("COUNT(g.id)")
                ->from("UserBundle:User", "u")
                ->join("u.groups g")
                ->where("g.id = :id")
                ->setParameter("id", $id);

        return $qb->getQuery()->getSingleScalarResult();
    }
}
