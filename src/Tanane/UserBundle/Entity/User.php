<?php

namespace Tanane\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="Tanane\UserBundle\Entity\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * Roles definition
     */
    const ROLE_PROFILE_ONE = 'Facturación y Entrega';

    const ROLE_PROFILE_TWO = 'Envío';

    const ROLE_ADMIN = 'Administrador';

    const ROLE_USER = 'No posee roles asignados';
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="Group")
     * @ORM\JoinTable(name="fos_user_user_group",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     * )
     */
    protected $groups;

    public function getId()
    {
        return $this->id;
    }

    public function getTransformedRoles()
    {
        $transformed = array();

        foreach ($this->getRoles() as $role) {
            $role = strtoupper($role);
            $const = sprintf('self::%s', $role);

            // Do not add if is $role === ROLE_USER
            if (BaseUser::ROLE_DEFAULT === $role) {
                continue;
            }

            if (!defined($const)) {
                throw \Exception(sprintf('User does not have the role constant "%s" set', $role));
            }

            $transformed[] = constant($const);
        }

        // If no roles add self::ROLE_USER
        if (empty($transformed)) {
            $transformed[] = self::ROLE_USER;
        }

        return $transformed;
    }
}
