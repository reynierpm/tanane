<?php

namespace Tanane\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('username', 'text')
                ->add('email', 'email')
                ->add('enabled', null, array(
                    'label' => 'Habilitado',
                    'required' => FALSE,
                ))
                ->add('plainPassword', 'repeated', array(
                    'type' => 'password',
                    'first_options' => array( 'label' => 'Contraseña' ),
                    'second_options' => array( 'label' => 'Repetir contraseña' ),
                    'invalid_message' => 'Las contraseñas no coinciden',
                    'required' => FALSE,
                ))
                ->add('roles', 'choice', array(
                    'required' => TRUE,
                    'choices' => array(
                        'ROLE_PROFILE_ONE' => 'Facturación y Entrega',
                        'ROLE_PROFILE_TWO' => 'Envío',
                        'ROLE_ADMIN' => 'Administrador',
                    ),
                    'expanded' => TRUE,
                    'multiple' => TRUE,
                ))
                ->add('groups', 'entity', array(
                    'class' => 'UserBundle:Group',
                    'multiple' => TRUE,
                    'expanded' => TRUE,
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Tanane\UserBundle\Entity\User',
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'user';
    }
}
