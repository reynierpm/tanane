<?php

namespace Tanane\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class GroupType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('name', 'text')
                ->add('roles', 'choice', array(
                    'required' => TRUE,
                    'choices' => array(
                        'ROLE_PROFILE_ONE' => 'Facturación y Entrega',
                        'ROLE_PROFILE_TWO' => 'Envío',
                        'ROLE_ADMIN' => 'Administrador',
                    ),
                    'expanded' => TRUE,
                    'multiple' => TRUE,
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Tanane\UserBundle\Entity\Group',
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'group';
    }
}
