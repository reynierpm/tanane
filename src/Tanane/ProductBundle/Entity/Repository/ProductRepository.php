<?php

namespace Tanane\ProductBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class ProductRepository extends EntityRepository
{
    public function findAll()
    {
        return $this->findBy(array(), array('id' => 'DESC', 'createdAt' => 'DESC'));
    }

    public function getProductByFilter($filter)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->select('p')->from("ProductBundle:Product", "p");
        $qb->where('p.product_name LIKE ?1');
        $qb->orWhere('p.product_description LIKE ?1');
        $qb->setParameter(1, '%'.$filter.'%');

        return $qb->getQuery()->getResult();
    }
}
