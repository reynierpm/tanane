<?php

namespace Tanane\ProductBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('product_name', 'text', array(
                    'required' => true,
                    'label' => "Nombre del Producto",
                    'trim' => true,
                ))
                ->add('product_description', 'textarea', array(
                    'required' => true,
                    'label' => "Descripción",
                    'trim' => true,
                ))
                ->add('active', null, array(
                    'required' => FALSE,
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
                array(
                    'data_class' => 'Tanane\ProductBundle\Entity\Product',
                    'intention' => 'product',
                )
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'product';
    }
}
