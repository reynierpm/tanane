<?php

namespace Tanane\ProductBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class ProductController extends Controller
{
    /**
     * @Route("/all_products", name="all_products")
     * @Method("GET")
     */
    public function allProductsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ProductBundle:Product')->findAll();

        $response['message'] = "";
        $response['entities'] = array();

        if (!$entities) {
            $response['message'] = "No se encontraron productos";
        }

        $helper = $this->container->get('vich_uploader.templating.helper.uploader_helper');

        foreach ($entities as $product) {
            $products = array();
            if ($product->getActive()) {
                $products['id'] = $product->getId();
                $products['name'] = $product->getProductName();
                $products['image'] = '<img src="'.$this->get('liip_imagine.cache.manager')->getBrowserPath($helper->asset($product, 'product_image'), 'tanane_thumb').'" class="img-thumbnail img-responsive" alt="'.$product->getImageName().'" title="'.$product->getImageName().'" />';

                $response['entities'][] = $products;
            }
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/get_products", name="get_products")
     * @Method("GET")
     */
    public function getProductByFilterAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ProductBundle:Product')->getProductByFilter($request->get('filter'));

        $response['items'] = array();

        if ($entities) {
            foreach ($entities as $product) {
                if ($product->getActive()) {
                    $products = array();
                    $products['data'] = $product->getId();
                    $products['value'] = $product->getProductName();
                    $products['url'] = '/uploads/products/'.$product->getImageName();
                    $response['items'][] = $products;
                }
            }
        }

        return new JsonResponse($response);
    }
}
