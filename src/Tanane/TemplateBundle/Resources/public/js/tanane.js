$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();

    $('select.toSelect2').select2();

    $('.img').on({
        'mouseover': function () {
            var img = '/bundles/frontend/images/' + $(this).attr('id') + '-1.jpg';
            $(this).attr('src', img);
        },
        mouseout: function () {
            var img = '/bundles/frontend/images/' + $(this).attr('id') + '.jpg';
            $(this).attr('src', img);
        }
    });

    //------- close alerts after 10 seconds -----------
    window.setTimeout(function () {
        $('.alert').delay(2000).fadeOut("slow", function () {
            $(this).remove();
        });
    }, 10000);

    //-------- natural form validation ----------------
    $('#naturalForm')
            .find('[name="orders[shipping_from]"]')
            .select2()
            .change(function (e) {
                $('#naturalForm').data('bootstrapValidator').disableSubmitButtons(false);
            })
            .end()
            .find('[name="canal_compra_nat"]')
            .select2()
            .change(function (e) {
                var $toggle = $($(this).attr('data-toggle'));
                $toggle.toggle();
                if (!$toggle.is(':visible')) {
                    $('#naturalForm').data('bootstrapValidator').disableSubmitButtons(false);
                }
            })
            .end()
            .find('[name="same_address"]')
            .click(function (e) {
                var $toggle = $($(this).attr('data-toggle'));
                $toggle.toggle();
                if ($toggle.is(':visible')) {
                    $('#naturalForm').data('bootstrapValidator').disableSubmitButtons(false);
                }
            })
            .end()
            .bootstrapValidator({
                feedbackIcons: {
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                },
                container: 'popover',
                fields: {
                    'orders[person][nat][description]': {
                        validators: {
                            notEmpty: {},
                            stringLength: {
                                min: 6
                            },
                            regexp: {
                                regexp: /^[a-zA-ZáéíóúñÁÉÍÓÚÑ.\s]+$/i,
                                message: 'Solo se permiten letras y espacios.'
                            }
                        }
                    },
                    'orders[nickname]': {
                        validators: {
                            notEmpty: {},
                            stringLength: {
                                min: 3
                            },
                        }
                    },
                    'orders[person][ci]': {
                        validators: {
                            notEmpty: {},
                            stringLength: {
                                min: 5
                            },
                            digits: {}
                        }
                    },
                    'orders[phone]': {
                        validators: {
                            noEmpty: {},
                            digits: {},
                            regexp: {
                                regexp: /^0(?:2(?:12|4[0-9]|5[1-9]|6[0-9]|7[0-8]|8[1-35-8]|9[1-5]|3[45789])|4(?:1[246]|2[46]))\d{7}$/,
                                message: 'El número de teléfono no es válido para Venezuela, por favor introduzca otro.'
                            }
                        }
                    },
                    'orders[email]': {
                        validators: {
                            notEmpty: {},
                            emailAddress: {}
                        }
                    },
                    'orders[fiscal_address]': {
                        validators: {
                            notEmpty: {}
                        }
                    },
                    'orders[order_amount]': {
                        validators: {
                            notEmpty: {},
                            numeric: {}
                        }
                    },
                    'orders[transaction]': {
                        validators: {
                            notEmpty: {},
                            digits: {}
                        }
                    }
                }
            })
            .on('success.form.bv', function (e) {
                e.preventDefault();
                var $form = $(e.target);
                var bv = $form.data('bootstrapValidator');
                bv.disableSubmitButtons(false);

                showLoader();
                $.post($form.attr('action'), $form.serialize(), 'json').done(function (data, textStatus, jqXHR) {
                    window.setTimeout(function () {
                        window.location = data.redirect_to;
                    }, 3000);
                }).fail(function () {
                    return false;
                }).always(hideLoader);
            });

    //-------- legal form validation ----------------
    $('#legalForm')
            .find('[name="canal_compra_nat"]')
            .select2()
            .change(function (e) {
                var $toggle = $($(this).attr('data-toggle'));
                $toggle.toggle();
                if (!$toggle.is(':visible')) {
                    $('#legalForm').data('bootstrapValidator').disableSubmitButtons(false);
                }
            })
            .end()
            .find('[name="same_address"]')
            .click(function (e) {
                var $toggle = $($(this).attr('data-toggle'));
                $toggle.toggle();
                if ($toggle.is(':visible')) {
                    $('#legalForm').data('bootstrapValidator').disableSubmitButtons(false);
                }
            })
            .end()
            .bootstrapValidator({
                feedbackIcons: {
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
                },
                container: 'popover',
                fields: {
                    'orders[person][leg][description]': {
                        validators: {
                            notEmpty: {},
                            stringLength: {
                                min: 6
                            },
                            regexp: {
                                regexp: /^[a-zA-ZáéíóúñÁÉÍÓÚÑ.\s]+$/i,
                                message: 'Solo se permiten letras y espacios.'
                            }
                        }
                    },
                    'orders[nickname]': {
                        validators: {
                            notEmpty: {},
                            stringLength: {
                                min: 3
                            }
                        }
                    },
                    'orders[person][rif]': {
                        validators: {
                            notEmpty: {},
                            stringLength: {
                                min: 5
                            },
                            digits: {},
                            rif: {
                                type: 'orders[person][identification_type]',
                                message: 'El RIF es incorrecto, por favor introduzca otro.'
                            }
                        }
                    },
                    'orders[phone]': {
                        validators: {
                            noEmpty: {},
                            digits: {},
                            regexp: {
                                regexp: /^0(?:2(?:12|4[0-9]|5[1-9]|6[0-9]|7[0-8]|8[1-35-8]|9[1-5]|3[45789])|4(?:1[246]|2[46]))\d{7}$/,
                                message: 'El número de teléfono no es válido para Venezuela, por favor introduzca otro.'
                            }
                        }
                    },
                    'orders[email]': {
                        validators: {
                            notEmpty: {},
                            emailAddress: {}
                        }
                    },
                    'orders[fiscal_address]': {
                        validators: {
                            notEmpty: {}
                        }
                    },
                    'orders[order_amount]': {
                        validators: {
                            notEmpty: {},
                            numeric: {}
                        }
                    },
                    'orders[transaction]': {
                        validators: {
                            notEmpty: {},
                            digits: {}
                        }
                    }
                }
            })
            .on('success.form.bv', function (e) {
                e.preventDefault();
                var $form = $(e.target);
                var bv = $form.data('bootstrapValidator');
                bv.disableSubmitButtons(false);

                showLoader();
                $.post($form.attr('action'), $form.serialize(), 'json').done(function (data, textStatus, jqXHR) {
                    window.setTimeout(function () {
                        window.location = data.redirect_to;
                    }, 3000);
                }).fail(function () {
                    return false;
                }).always(hideLoader);
            });

            function showLoader() {
                $('#blockPanel').show();
            }

            function hideLoader() {
                $('#blockPanel').hide();
            }
    function productFormatResult(product) {
        var markup = '<img style="height: 40px;width: 40px;" src="' +
                product.url + '" class="img-rounded" id="ProductoForm_0_image" />&nbsp;&nbsp;' + product.value;
        return markup;
    }

    function productFormatSelection(product) {
        return product.value;
    }

    $("#ProductoForm_0_product_name").select2({
        placeholder: "Buscar producto",
        minimumInputLength: 0,
        id: function (prod) {
            return prod.value;
        },
        ajax: {
            url: Routing.generate('get_products'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term, page) {
                return {
                    filter: term,
                    page: page
                };
            },
            results: function (data, page) {
                var more = (page * 30) < data.total_count;
                return {results: data.items, more: more};
            }
        },
        formatResult: productFormatResult,
        formatSelection: productFormatSelection,
        escapeMarkup: function (m) {
            return m;
        },
        formatNoResults: function () {
            return "No se encontraron productos para la palabra actual";
        },
        formatAjaxError: function () {
            return "No hay conexión con el servidor";
        }
    });

    $('#verticalForm').on('click', '.add-link', function (event) {
        var num = 0;
        if (isNaN($('.products').last().data('num')) == true) {
            num = 0;
        } else {
            num = parseInt($('.products').last().data('num')) + 1;
        }

        $('#products_forms').append($('#clone').data('content').replace(/kk/g, num));
        $('#ProductoForm_' + num + '_product_name').select2({
            placeholder: "Buscar producto",
            minimumInputLength: 0,
            id: function (prod) {
                return prod.value;
            },
            ajax: {
                url: Routing.generate('get_products'),
                dataType: 'json',
                quietMillis: 250,
                data: function (term, page) {
                    return {
                        filter: term,
                        page: page
                    };
                },
                results: function (data, page) {
                    var more = (page * 30) < data.total_count;
                    return {results: data.items, more: more};
                }
            },
            formatResult: productFormatResult,
            formatSelection: productFormatSelection,
            escapeMarkup: function (m) {
                return m;
            },
            formatNoResults: function () {
                return "No se encontraron productos para la palabra actual";
            },
            formatAjaxError: function () {
                return "No hay conexión con el servidor";
            }
        });
        event.preventDefault();
    });

    $('#verticalForm').on('click', '.close-link', function (event) {
        if (!confirm('¿Está seguro que desea borrar este elemento?'))
            return false;
        if ($('.products').length > 1) {
            $($(this).data('content')).remove();
        }
        event.preventDefault();
    });

    // Remove MRW if checkbox is checked
    var detachedMember;
    $('.lives_in_ccs').click(function () {
        if (this.checked) {
            // After removing the element, reset the value to `--SELECCIONAR--` (fix the issue for send empty values)
            if ($('.shipping_from').select2('val') == 'MRW') {
                $('.shipping_from').select2('val', '');
            }
            detachedMember = $('.shipping_from option[value="MRW"]').detach();
        } else {
            $('.shipping_from option[value=""]').after(detachedMember);
        }

        $(".secure_shipping").toggle(this.checked);
    });

    // We can secure our shipping since courriers allow it
    $(".shipping_from").on('change', function () {
        option = $('.shipping_from :selected').val();
        if (option === "MRW")
        {
            $("#mrw").show();
            $(".secure_shipping").toggle(false);
        }
        else
        {
            $("#mrw").hide();
            $(".secure_shipping").toggle(true);
        }
    });

    // Toggle display nickname if users pick "Mercado Libre"
    $("#canal_compra_nat").on('change', function () {
        option = $('#canal_compra_nat :selected').val();
        if (option == "1") {
            $("div#nickname_nat").show();
        } else {
            $("div#nickname_nat").hide();
        }
    });

    $("#canal_compra_jur").on('change', function () {
        option = $('#canal_compra_jur :selected').val();
        if (option == "1") {
            $(".nickname_jur").show();
        } else {
            $(".nickname_jur").hide();
        }
    });

    // Disabled shipping_address if checkbox is checked
    $("#same_address").click(function () {
        var $toggle = $($(this).attr('data-toggle'));
        $toggle.toggle(!this.checked);
    });
});
