<?php

namespace Tanane\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Tanane\CommonBundle\Entity\PaymentType;
use Tanane\CommonBundle\Form\PaymentTypeType;

class PaymentTypeController extends Controller
{
    /**
     * @Route("/admin/payment-type/list", name="list-payment-type")
     * @Security("has_role('ROLE_ADMIN')")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }

    /**
     * @Route("/admin/payment-type/get-list", name="get-list-payment-type")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function getPaymentTypeAction()
    {
        $response = array();
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository("CommonBundle:PaymentType")->findAll();

        $payments = array();
        foreach ($entities as $entity) {
            $payment = array();
            $payment[] = $entity->getActive() == FALSE ? '<i class="fa fa-times-circle-o fa-2x text-danger"></i>' : '<i class="fa fa-check-circle-o text-success fa-2x"></i>';
            $payment[] = $entity->getName();
            $payment[] = '<a rel="tooltip" data-placement="top" data-original-title="Modificar" class="btn btn-primary" href="'.$this->generateUrl('edit-payment-type', array('id' => $entity->getId()), true).'"><i class="fa fa-edit"></i></a> <a rel="tooltip" data-placement="top" data-original-title="Eliminar" class="btn btn-danger confirm-delete" data-id="'.$entity->getId().'" href="#"><i class="fa fa-trash-o"></i></a>';

            $payments[] = $payment;
        }

        $response['data'] = $payments;

        return new JsonResponse($response);
    }

    /**
     * @Route("/admin/payment-type/new", name="new-payment-type")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function newAction()
    {
        $entity = new PaymentType();
        $form = $this->createForm(new PaymentTypeType(), $entity);

        return array('entity' => $entity, 'form' => $form->createView());
    }

    /**
     * @Route("/admin/payment-type/save", name="save-payment-type")
     * @Method("POST")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function saveAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new PaymentType();

        $form = $this->createForm(new PaymentTypeType(), $entity);
        $form->handleRequest($request);

        $response = array();
        $response['status'] = true;

        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $response['message'] = $this->get('translator')->trans('create.success', array('%element%' => 'el tipo de pago'));
        } else {
            $response['error'] = $this->getFormErrors($form);
            $response['message'] = $this->get('translator')->trans('create.fail', array('%element%' => 'el tipo de pago'));
            $this->get('ladybug')->log($response['error']);
            $response['status'] = false;
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/admin/payment-type/edit/{id}", name="edit-payment-type")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editAction($id = null)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CommonBundle:PaymentType')->find($id);
        $form = $this->createForm(new PaymentTypeType(), $entity, array('action' => "#", 'method' => 'POST'));

        return array(
            'form' => $form->createView(),
            'id' => $id,
        );
    }

    /**
     * @Route("/admin/payment-type/update/{id}", name="update-payment-type")
     * @Method("POST")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function updateAction(Request $request, $id = null)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository("CommonBundle:PaymentType")->find($id);

        $form = $this->createForm(new PaymentTypeType(), $entity);
        $form->handleRequest($request);

        $response = array();
        $response['status'] = true;

        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $response['message'] = $this->get('translator')->trans('update.success', array('%element%' => 'tipo de pago'));
        } else {
            $response['message'] = $this->get('translator')->trans('update.fail', array('%element%' => 'tipo de pago'));
            $response['error'] = $this->getFormErrors($form);
            $this->get('ladybug')->log($response['error']);
            $response['status'] = false;
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/admin/payment-type/delete/{id}", name="delete-payment-type")
     * @Method({"DELETE","GET"})
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CommonBundle:PaymentType')->find($id);
        $response = array();
        $response["status"] = true;

        if ($entity) {
            $em->remove($entity);
            $em->flush();
            $response['message'] = $this->get('translator')->trans('delete.success', array('%element%' => 'tipo de pago'));
        } else {
            $response['message'] = $this->get('translator')->trans('delete.fail', array('%element%' => 'tipo de pago'));
            $response["status"] = false;
        }

        return new JsonResponse($response);
    }

    /**
     * @param  \Symfony\Component\Form\Form $form
     * @return array
     */
    private function getFormErrors(\Symfony\Component\Form\Form $form)
    {
        $errors = array();

        foreach ($form->getErrors() as $key => $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getFormErrors($child);
            }
        }

        return $errors;
    }
}
