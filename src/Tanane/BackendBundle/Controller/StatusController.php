<?php

namespace Tanane\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Tanane\CommonBundle\Entity\Status;
use Tanane\CommonBundle\Form\StatusType;

class StatusController extends Controller
{
    /**
     * @Route("/admin/status/list", name="list-status")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }

    /**
     * @Route("/admin/status/new", name="new-status")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Status();
        $form = $this->createForm(new StatusType(), $entity, array('action' => $this->generateUrl('save-status')));

        return array('entity' => $entity, 'form' => $form->createView());
    }

    /**
     * @Route("/admin/status/edit/{id}", name="edit-status")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editAction($id = null)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CommonBundle:Status')->find($id);
        $form = $this->createForm(new StatusType(), $entity, array(
            'action' => $this->generateUrl('update-status', array('id' => $id)),
            'method' => 'POST',
        ));

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
            'id' => $id,
        );
    }

    /**
     * @Route("/admin/status/delete/{id}", name="delete-status")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CommonBundle:Status')->find($id);
        $response = array();
        if ($entity) {
            $em->remove($entity);
            $em->flush();
            $response["message"] = 'Estatus eliminado satisfactoriamente';

            return $this->redirect($this->generateUrl('list-status'));
        } else {
            $response["message"] = "El Estatus no se encuentra";
            $response["error"] = true;
        }

        return new JsonResponse(array("response" => $response));
    }

    /**
     * @Route("/admin/status/get-list", name="get-list-status")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function getStatusAction()
    {
        $response = array();
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository("CommonBundle:Status")->findAll();

        $payments = array();
        foreach ($entities as $entity) {
            $payment = array();
            $payment[] = $entity->getName();
            $payment[] = '<a class="btn btn-default btn-sm" href="'.$this->generateUrl('edit-status', array('id' => $entity->getId()), true).'"><i class="fa fa-pencil-square-o"></i> Editar</a> <a class="btn btn-danger btn-sm" href="'.$this->generateUrl('delete-status', array('id' => $entity->getId()), true).'"><i class="fa fa-times-circle-o"></i> Eliminar</a>';

            $payments[] = $payment;
        }

        $response['data'] = $payments;

        return new JsonResponse($response);
    }

    /**
     * @Route("/admin/status/save", name="save-status")
     * @Method("POST")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function saveAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = new Status();
        $form = $this->createForm(new StatusType(), $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $flashMessage = "Se ha agregado el Estatus satisfactoriamente";
            $this->get('session')->getFlashBag()->add('message', $flashMessage);

            return $this->redirect($this->generateUrl('list-status'));
        } else {
            $errors = $this->getFormErrors($form);
        }

        return new JsonResponse(array('status' => true, 'errors' => $errors));
    }

    /**
     * @Route("/admin/status/update/{id}", name="update-status")
     * @Method("POST")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function updateAction(Request $request, $id = null)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository("CommonBundle:Status")->find($id);
        $form = $this->createForm(new StatusType(), $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $flashMessage = "Se ha actualizado el Estatus satisfactoriamente";
            $this->get('session')->getFlashBag()->add('message', $flashMessage);

            return $this->redirect($this->generateUrl('list-status'));
        } else {
            $errors = $this->getFormErrors($form);
        }

        return new JsonResponse(array('status' => true, 'errors' => $errors));
    }

    /**
     * @Route("/all_status", name="all_status")
     * @Security("has_role('ROLE_ADMIN')")
     * @Method("GET")
     */
    public function allStatusAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository("CommonBundle:Status")->findAll();

        $response['message'] = "";
        $response['entities'] = array();

        foreach ($entities as $status) {
            $statuss = array();
            $statuss['id'] = $status->getId();
            $statuss['name'] = $status->getName();

            $response['entities'][] = $statuss;
        }

        return new JsonResponse($response);
    }

    /**
     * @param  \Symfony\Component\Form\Form $form
     * @return array
     */
    private function getFormErrors(\Symfony\Component\Form\Form $form)
    {
        $errors = array();

        foreach ($form->getErrors() as $key => $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getFormErrors($child);
            }
        }

        return $errors;
    }
}
