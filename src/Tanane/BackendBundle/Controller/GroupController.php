<?php

namespace Tanane\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Tanane\UserBundle\Entity\Group;
use Tanane\UserBundle\Form\Type\GroupType;

class GroupController extends Controller
{
    /**
     * @Route("/admin/group/list", name="list-group")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }

    /**
     * @Route("/admin/group/get-list", name="get-list-group")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function getGroupsAction()
    {
        $response = array();
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository("UserBundle:Group")->findAll();

        $groups = array();
        foreach ($entities as $entity) {
            $group = array();
            $group[] = $entity->getName();
            $group[] = $entity->getRoles() != NULL ? $entity->getRoles() : "-";
            $group[] = '<a rel="tooltip" data-placement="top" data-original-title="Modificar" class = "btn btn-primary" href="'.$this->generateUrl('edit-group', array('id' => $entity->getId()), true).'"><i class="fa fa-pencil-square-o"></i></a> <a rel="tooltip" data-placement="top" data-original-title="Eliminar" class="btn btn-danger confirm-delete" data-id="'.$entity->getId().'" data-name="'.$entity->getName().'" href ="#"><i class="fa fa-trash-o"></i></a>';

            $groups[] = $group;
        }

        $response['data'] = $groups;

        return new JsonResponse($response);
    }

    /**
     * @Route("/admin/group/new", name="new-group")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Group('');
        $form = $this->createForm(new GroupType(), $entity);

        return array('entity' => $entity, 'form' => $form->createView());
    }

    /**
     * @Route("/admin/group/save", name="save-group")
     * @Method("POST")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function saveAction(Request $request)
    {
        $entity = new Group('');
        $form = $this->createForm(new GroupType(), $entity);
        $form->handleRequest($request);
        $response = array();
        $response['status'] = true;

        /** @var $groupManager \FOS\UserBundle\Model\GroupManagerInterface */
        $groupManager = $this->container->get('fos_user.group_manager');
        $group = $groupManager->createGroup('');

        if ($form->isValid()) {
            $group_values = $request->get('group');
            $group->setName($group_values['name']);

            if (isset($group_values['roles'])) {
                $group->setRoles($group_values['roles']);
            }

            $groupManager->updateGroup($group);

            $response['message'] = $this->get('translator')->trans('create.success', array('%element%' => 'el grupo'));
        } else {
            $response['message'] = $this->get('translator')->trans('create.fail', array('%element%' => 'el grupo'));
            $response['error'] = $this->getFormErrors($form);
            $this->get('ladybug')->log($response['error']);
            $response['status'] = false;
        }

        return new JsonResponse($response);
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/admin/group/{id}/edit", name="edit-group")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $group = $em->getRepository('UserBundle:Group')->find($id);

        $form = $this->createForm(new GroupType(), $group, array('action' => "#", 'method' => 'POST'));

        return array(
            'form' => $form->createView(),
            'group' => $group,
        );
    }

    /**
     * @Route("/admin/group/update/{id}", name="update-group")
     * @Method("POST")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function updateAction(Request $request, $id = null)
    {
        $em = $this->getDoctrine()->getManager();
        $group = $em->getRepository("UserBundle:Group")->find($id);

        $form = $this->createForm(new GroupType(), $group);
        $form->handleRequest($request);

        $response = array();
        $response['status'] = true;

        if ($form->isValid()) {
            $groupManager = $this->container->get('fos_user.group_manager');
            $groupManager->updateGroup($group);
            $response['message'] = $this->get('translator')->trans('update.success', array('%element%' => 'grupo'));
        } else {
            $response['message'] = $this->get('translator')->trans('update.fail', array('%element%' => 'grupo'));
            $response['error'] = $this->getFormErrors($form);
            $this->get('ladybug')->log($response['error']);
            $response['status'] = false;
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/admin/group/delete/{id}", name="delete-group")
     * @Method({"DELETE","GET"})
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $groupIsAssigned = $em->getRepository('UserBundle:Group')->groupIsAssigned($id);

        $response = array();
        $response["status"] = true;

        if ($groupIsAssigned) {
            $response['message'] = $this->get('translator')->trans('delete.fail', array('%element%' => 'grupo'));
            $response["status"] = false;
            $response["assigned"] = true;
        } else {
            $entity = $em->getRepository("UserBundle:Group")->findOneBy(array('id' => $id));

            if ($entity) {
                $groupManager = $this->container->get('fos_user.group_manager');
                $groupManager->deleteGroup($entity);
                $response['message'] = $this->get('translator')->trans('delete.success', array('%element%' => 'grupo'));
            } else {
                $response['message'] = $this->get('translator')->trans('delete.fail', array('%element%' => 'grupo'));
                $response["status"] = false;
            }
        }

        return new JsonResponse($response);
    }

    /**
     * @param  \Symfony\Component\Form\Form $form
     * @return array
     */
    private function getFormErrors(\Symfony\Component\Form\Form $form)
    {
        $errors = array();

        foreach ($form->getErrors() as $key => $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getFormErrors($child);
            }
        }

        return $errors;
    }
}
