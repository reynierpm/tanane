<?php

namespace Tanane\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Tanane\ProductBundle\Entity\Product;
use Tanane\ProductBundle\Form\Type\ProductFormType;

class ProductController extends Controller
{
    /**
     * @Route("/admin/product/list", name="list-product")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository("ProductBundle:Product")->findAll();

        return array('entities' => $entities);
    }

    /**
     * @Route("/admin/product/new", name="new-product")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Product();
        $form = $this->createForm(new ProductFormType(), $entity);

        return array('entity' => $entity, 'form' => $form->createView());
    }

    /**
     * @Route("/admin/product/save", name="save-product")
     * @Method("POST")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function saveAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new Product();

        $response = array();
        $response['status'] = true;

        $form = $this->createForm(new ProductFormType(), $entity);
        $form->handleRequest($request);

        $picturePath = $this->container->getParameter('kernel.root_dir').'/../web/uploads/products/';

        if (!empty($_POST['img'])) {
            $img = $_POST['img'];

            $b64 = explode(",", $img);
            $arrExt = explode("/", $b64 [0]);
            $extension = explode(";", $arrExt [1]);
            $filename = 'product_'.sha1(uniqid(mt_rand(), true));
            $filename = $filename.'.'.$extension [0];

            $file = fopen($picturePath.$filename, "wb");
            fwrite($file, base64_decode(str_replace(" ", "+", $b64 [1])));
            fclose($file);

            $entity->setImageName($filename);
        } else {
            $entity->setImageName(null);
        }

        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $response['data'] = $this->get('translator')->trans('create.success', array('%element%' => 'el producto'));
        } else {
            // $response[ 'data' ] = $this->getFormErrors($form);
            $response['data'] = $this->get('translator')->trans('create.fail', array('%element%' => 'el producto'));
            // $this->get('ladybug')->log($response[ 'error' ]);
            $response['status'] = false;
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/admin/product/edit/{id}", name="edit-product")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository("ProductBundle:Product")->findOneBy(array("id" => $id));
        $form = $this->createForm(new ProductFormType(), $entity, array('action' => "#", 'method' => 'POST'));

        return array(
            'form' => $form->createView(),
            'id' => $id,
            'producto' => $entity,
        );
    }

    /**
     * @Route("/admin/product/update/{id}", name="update-product")
     * @Method("POST")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function updateAction(Request $request, $id = null)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository("ProductBundle:Product")->find($id);
        $product = $request->get('product');

        $form = $this->createForm(new ProductFormType(), $entity);
        $form->handleRequest($request);

        $picturePath = $this->container->getParameter('kernel.root_dir').'/../web/uploads/products/';

        if (!isset($_POST['img'])) {
            $img = $_POST['img'];

            $b64 = explode(",", $img);
            $arrExt = explode("/", $b64 [0]);
            $extension = explode(";", $arrExt [1]);
            $filename = 'product_'.sha1(uniqid(mt_rand(), true));
            $filename = $filename.'.'.$extension [0];

            $file = fopen($picturePath.$filename, "wb");
            fwrite($file, base64_decode(str_replace(" ", "+", $b64 [1])));
            fclose($file);

            $entity->setImageName($filename);
        }

        $response = array();
        $response['status'] = true;

        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $response['data'] = $this->get('translator')->trans('update.success', array('%element%' => 'producto'));
        } else {
            $response['data'] = $this->get('translator')->trans('update.fail', array('%element%' => 'producto'));
            $response['status'] = false;
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/admin/product/delete/{id}", name="delete-product")
     * @Method({"GET", "DELETE"})
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $orderHasProduct = $em->getRepository("FrontendBundle:OrderHasProduct")->findBy(array('product' => $id));

        $response = array();
        $response["status"] = true;
        $response["assigned"] = true;

        if ($orderHasProduct) {
            $response['message'] = $this->get('translator')->trans('delete.fail', array('%element%' => 'producto. Se encuentra en una Orden.'));
            $response["status"] = false;
            $response["assigned"] = true;
        } else {
            $entity = $em->getRepository("ProductBundle:Product")->findOneBy(array('id' => $id));

            if ($entity) {
                $em->remove($entity);
                $em->flush();
                $response['message'] = $this->get('translator')->trans('delete.success', array('%element%' => 'producto'));
                $response["assigned"] = false;
            } else {
                $response['message'] = $this->get('translator')->trans('delete.fail', array('%element%' => 'producto'));
                $response["status"] = false;
            }
        }

        return new JsonResponse($response);
    }

    /**
     * @param  \Symfony\Component\Form\Form $form
     * @return array
     */
    private function getFormErrors(\Symfony\Component\Form\Form $form)
    {
        $errors = array();

        foreach ($form->getErrors() as $key => $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getFormErrors($child);
            }
        }

        return $errors;
    }
}
