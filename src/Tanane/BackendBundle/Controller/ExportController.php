<?php

namespace Tanane\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Tanane\FrontendBundle\Entity\NaturalPerson;
use JMS\SecurityExtraBundle\Annotation\Secure;

class ExportController extends Controller
{
    /**
     * @Route("/admin/genPDF", name="genPDF")
     * @Method("POST")
     * @Secure(roles="IS_AUTHENTICATED_FULLY, ROLE_PROFILE_ONE, ROLE_PROFILE_TWO, ROLE_ADMIN")
     * @Template()
     */
    public function generatePdfAction(Request $request)
    {
        $imprimir = $request->get('print');
        if (count($imprimir) > 0) {
            $pdf = $this->container->get("white_october.tcpdf")->create();
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->setPrintHeader(false);
            $pdf->setPrintFooter(false);
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            $pdf->SetMargins(5, 20, 5);
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
            $pdf->SetFont('dejavusans', '', 15, '', true);
            $pdf->AddPage();
            $pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => array(196, 196, 196), 'opacity' => 1, 'blend_mode' => 'Normal'));
            $tabla = '';
            $em = $this->getDoctrine()->getManager();
            $i = 0;
            foreach ($imprimir as $id => $value) {
                $entity = $em->getRepository('FrontendBundle:Orders')->find($id);
                $person = $entity->getPerson();
                if ($person instanceof NaturalPerson) {
                    $cedula = $person->getIdentificationType() . $person->getCI();
                } else {
                    $cedula = $person->getIdentificationType() . $person->getRIF();
                }
                $tabla = '<hr /><br><br><br>
                    <table width="800" border="0" >
                      <tr>
                        <th align="right" valign="top" scope="row"><strong>NOMBRES Y APELLIDOS:</strong></th>
                        <td width="10">&nbsp;</td>
                        <td align="left" valign="top" width="auto"><i>' . ucwords(strtolower($entity->getPerson()->getDescription())) . '</i></td>
                      </tr>
                      <tr>
                        <th align="right" valign="top" scope="row"><strong>CÉDULA:</strong></th>
                        <td width="10">&nbsp;</td>
                        <td align="left" valign="top" width="auto"><i>' . $cedula . '</i></td>
                      </tr>
                      <tr>
                        <th  align="right" valign="top" scope="row"><strong>TELÉFONO:</strong></th>
                        <td width="10">&nbsp;</td>
                        <td align="left" valign="top" width="auto"><i>' . $this->container->get('libphonenumber.phone_number_util')->format($entity->getPhone(), 'NATIONAL') . '</i></td>
                      </tr>
                      <tr>
                        <th  align="right" valign="top" scope="row"><strong>PERSONA DE CONTACTO:</strong></th>
                        <td width="10">&nbsp;</td>
                        <td align="left" valign="top" width="auto"><i>' . ucwords(strtolower($entity->getPerson()->getContactPerson())) . '</i></td>
                      </tr>
                      <tr>
                        <th  align="right" valign="top" scope="row"><strong>DIRECCIÓN DE ENVÍO :</strong></th>
                        <td width="10">&nbsp;</td>
                        <td align="left" valign="top" width="auto"><i>' . $entity->getShippingAddress() . '</i></td>
                      </tr>
                      <tr>
                        <th  align="right" valign="top" scope="row"><strong>EMPRESA DE ENVÍO:</strong></th>
                        <td width="10">&nbsp;</td>
                        <td align="left" valign="top" width="auto"><i>' . $entity->getShippingFrom() . '</i></td>
                      </tr>
                    </table><br><br><br><hr />';
                $pdf->writeHTML($tabla, true, false, true, false, '');
                $i++;
                if ($i > 2) {
                    $pdf->AddPage();
                    $i = 0;
                }
            }
            $pdf->Output('ordenes.pdf', 'I');
        } else {
            return $this->redirect($this->generateUrl('list-order'));
        }
    }

}