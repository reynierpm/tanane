<?php

namespace Tanane\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Tanane\CommonBundle\Entity\Bank;
use Tanane\CommonBundle\Form\BankType;

class BankController extends Controller
{
    /**
     * @Route("/admin/bank/list", name="list-bank")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }

    /**
     * @Route("/admin/bank/new", name="new-bank")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Bank();
        $form = $this->createForm(new BankType(), $entity);

        return array('entity' => $entity, 'form' => $form->createView());
    }

    /**
     * @Route("/admin/bank/get-list", name="get-list-bank")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function getBanksAction()
    {
        $response = array();
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository("CommonBundle:Bank")->findAll();

        $banks = array();
        foreach ($entities as $entity) {
            $bank = array();
            $bank[] = $entity->getActive() == FALSE ? '<i class="fa fa-times-circle-o fa-2x text-danger"></i>' : '<i class="fa fa-check-circle-o text-success fa-2x"></i>';
            $bank[] = $entity->getName();
            $bank[] = $entity->getCode();
            $bank[] = '<a rel="tooltip" data-placement="top" data-original-title="Modificar" class = "btn btn-primary" href="'.$this->generateUrl('edit-bank', array('id' => $entity->getId()), true).'"><i class="fa fa-pencil-square-o"></i></a> <a rel="tooltip" data-placement="top" data-original-title="Eliminar" class="btn btn-danger confirm-delete" data-id="'.$entity->getId().'" href ="#"><i class="fa fa-trash-o"></i></a>';

            $banks[] = $bank;
        }

        $response['data'] = $banks;

        return new JsonResponse($response);
    }

    /**
     * @Route("/admin/bank/save", name="save-bank")
     * @Method("POST")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function saveAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new Bank();
        $form = $this->createForm(new BankType(), $entity);
        $form->handleRequest($request);
        $response = array();
        $response['status'] = true;

        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $response['message'] = $this->get('translator')->trans('create.success', array('%element%' => 'el banco'));
        } else {
            $response['error'] = $this->getFormErrors($form);
            $response['message'] = $this->get('translator')->trans('create.fail', array('%element%' => 'el banco'));
            $this->get('ladybug')->log($response['error']);
            $response['status'] = false;
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/admin/bank/edit/{id}", name="edit-bank")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editAction($id = null)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CommonBundle:Bank')->find($id);
        $form = $this->createForm(new BankType(), $entity, array('action' => '#', 'method' => 'POST'));

        return array(
            'form' => $form->createView(),
            'id' => $id,
        );
    }

    /**
     * @Route("/admin/bank/update/{id}", name="update-bank")
     * @Method("POST")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function updateAction(Request $request, $id = null)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository("CommonBundle:Bank")->find($id);

        $form = $this->createForm(new BankType(), $entity);
        $form->handleRequest($request);

        $response = array();
        $response['status'] = true;

        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $response['message'] = $this->get('translator')->trans('update.success', array('%element%' => 'banco'));
        } else {
            $response['message'] = $this->get('translator')->trans('update.fail', array('%element%' => 'banco'));
            $response['error'] = $this->getFormErrors($form);
            $this->get('ladybug')->log($response['error']);
            $response['status'] = false;
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/admin/bank/delete/{id}", name="delete-bank")
     * @Method({"DELETE","GET"})
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CommonBundle:Bank')->find($id);
        $response = array();
        $response["status"] = true;

        if ($entity) {
            $em->remove($entity);
            $em->flush();
            $response['message'] = $this->get('translator')->trans('delete.success', array('%element%' => 'banco'));
        } else {
            $response['message'] = $this->get('translator')->trans('delete.fail', array('%element%' => 'banco'));
            $response["status"] = false;
        }

        return new JsonResponse($response);
    }

    /**
     * @param  \Symfony\Component\Form\Form $form
     * @return array
     */
    private function getFormErrors(\Symfony\Component\Form\Form $form)
    {
        $errors = array();

        foreach ($form->getErrors() as $key => $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getFormErrors($child);
            }
        }

        return $errors;
    }
}
