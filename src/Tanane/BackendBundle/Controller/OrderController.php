<?php

namespace Tanane\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Tanane\FrontendBundle\Form\Type\OrdersType;
use Tanane\FrontendBundle\Entity\NaturalPerson;
use Tanane\FrontendBundle\Entity\Orders;
use Tanane\FrontendBundle\Entity\OrderHasProduct;
use JMS\SecurityExtraBundle\Annotation\Secure;

class OrderController extends Controller
{
    /**
     * @Route("/admin/order/list", name="list-order")
     * @Route("/admin", name="admin")
     * @Method("GET")
     * @Secure(roles="IS_AUTHENTICATED_FULLY, ROLE_PROFILE_ONE, ROLE_PROFILE_TWO, ROLE_ADMIN")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }

    /**
     * @Route("/admin/order/get-list", name="get-list-order")
     * @Method("GET")
     * @Secure(roles="IS_AUTHENTICATED_FULLY, ROLE_PROFILE_ONE, ROLE_PROFILE_TWO, ROLE_ADMIN")
     * @Template()
     */
    public function getOrdersAction()
    {
        $response = array();
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository("FrontendBundle:Orders")->findAll();

        $orders = array();
        foreach ($entities as $entity) {
            $person = $entity->getPerson();
            $order = array();
            $order[] = '<label class="checkbox"><input name="print['.$entity->getId().']"  id="print_'.$entity->getId().'" type="checkbox"><i></i></label>';
            $order[] = $entity->getCreatedAt()->format('d/m/Y, g:i a ');
            $order[] = $entity->getPerson()->getDescription();
            if ($person instanceof NaturalPerson) {
                $id = $person->getIdentificationType().$person->getCI();
            } else {
                $id = $person->getIdentificationType().$person->getRIF();
            }
            $order[] = $id;
            $order[] = $entity->getEmail();
            $order[] = $this->container->get('libphonenumber.phone_number_util')->format($entity->getPhone(), 'NATIONAL');
            $order[] = $this->status($entity->getStatus()->getId());
            $order[] = '<a rel="tooltip" data-placement="top" data-original-title="Modificar" class = "btn btn-primary" href = "'.$this->generateUrl('edit-order', array( 'id' => $entity->getId() ), true).'"><i class = "fa fa-edit"></i></a> <a rel="tooltip" data-placement="top" data-original-title="Eliminar" class="btn btn-danger confirm-delete" href="#" data-id="'.$entity->getId().'"><i class="fa fa-trash-o"></i></a>';

            $orders[] = $order;
        }

        $response[ 'data' ] = $orders;

        return new JsonResponse($response);
    }

    private function status($status_id)
    {
        $status = '';
        switch ($status_id) {
            case 1 :
                $status = '&nbsp;<i  rel="tooltip" data-placement="top" data-original-title="Leído" class="fa fa-times-circle-o fa-2x text-danger tooltip-status"></i>';
                $status .= '&nbsp;<i rel="tooltip" data-placement="top" data-original-title="Facturado" class="fa fa-times-circle-o fa-2x text-danger tooltip-status"></i>';
                $status .= '&nbsp;<i rel="tooltip" data-placement="top" data-original-title="Enviado" class="fa fa-times-circle-o fa-2x text-danger tooltip-status"></i>';
                break;
            case 2 :
                $status = '&nbsp;<i  rel="tooltip" data-placement="top" data-original-title="Leído" class="fa fa-check-circle-o fa-2x text-success tooltip-status"></i>';
                $status .= '&nbsp;<i rel="tooltip" data-placement="top" data-original-title="Facturado" class="fa fa-times-circle-o fa-2x text-danger tooltip-status"></i>';
                $status .= '&nbsp;<i rel="tooltip" data-placement="top" data-original-title="Enviado" class="fa fa-times-circle-o fa-2x text-danger tooltip-status"></i>';
                break;
            case 3 :
                $status = '&nbsp;<i  rel="tooltip" data-placement="top" data-original-title="Leído" class="fa fa-check-circle-o fa-2x text-success tooltip-status"></i>';
                $status .= '&nbsp;<i rel="tooltip" data-placement="top" data-original-title="Facturado" class="fa fa-check-circle-o fa-2x text-success tooltip-status"></i>';
                $status .= '&nbsp;<i rel="tooltip" data-placement="top" data-original-title="Enviado" class="fa fa-times-circle-o fa-2x text-danger tooltip-status"></i>';
                break;
            case 4 :
                $status = '&nbsp;<i  rel="tooltip" data-placement="top" data-original-title="Leído" class="fa fa-check-circle-o fa-2x text-success tooltip-status"></i>';
                $status .= '&nbsp;<i rel="tooltip" data-placement="top" data-original-title="Facturado" class="fa fa-check-circle-o fa-2x text-success tooltip-status"></i>';
                $status .= '&nbsp;<i rel="tooltip" data-placement="top" data-original-title="Enviado" class="fa fa-check-circle-o fa-2x text-success tooltip-status"></i>';
                break;
        }

        return $status;
    }

    /**
     * @Route("/admin/order/edit/{id}", name="edit-order")
     * @Method("GET")
     * @Secure(roles="IS_AUTHENTICATED_FULLY, ROLE_PROFILE_ONE, ROLE_PROFILE_TWO, ROLE_ADMIN")
     * @Template()
     */
    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $order = $em->getRepository('FrontendBundle:Orders')->find($id);

        $params = explode('::', $request->attributes->get('_controller'));
        $actionName = substr($params[ 1 ], 0, -6);

        $orderForm = $this->createForm(new OrdersType(), $order, array( 'register_type' => $order->getPerson()->getPersonType(), 'curr_action' => $actionName ));
        $products = $em->getRepository("FrontendBundle:Orders")->getOrderProducts($id);

        return array(
            "form" => $orderForm->createView(),
            'id' => $id,
            'entity' => $order,
            'products' => $products,
        );
    }

    /**
     * @Route("/admin/order/update/{id}", name="update-order")
     * @Method("POST")
     * @Secure(roles="IS_AUTHENTICATED_FULLY, ROLE_PROFILE_ONE, ROLE_PROFILE_TWO, ROLE_ADMIN")
     * @Template()
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository("FrontendBundle:Orders")->find($id);
        $person = $entity->getPerson()->getPersonType() === 1 ? "natural" : "legal";

        $params = explode('::', $request->attributes->get('_controller'));
        $actionName = substr($params[ 1 ], 0, -6);

        $form = $this->createForm(new OrdersType(), $entity, array( 'register_type' => $person, 'curr_action' => $actionName ));
        $form->handleRequest($request);

        $response = array();
        $response[ 'status' ] = true;

        $status = $em->getRepository("CommonBundle:OrderStatus")->findOneBy(array( "id" => $request->get('orders')[ 'status' ] ));

        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $informacion = array( 'username' => $entity->getPerson()->getDescription() );
            $template = '';
            $subject = '';

            if ($entity->getInvoiceNo() != '' && $entity->getShipmentNo() == '') {
                $informacion['invoice_no'] = $entity->getInvoiceNo();
                $template = 'FrontendBundle:Mail:invoice.txt.twig';
                $subject = 'Pedido Facturado';
            }

            if ($entity->getInvoiceNo() != '' && $entity->getShipmentNo() != '') {
                $informacion['shipment_no'] = $entity->getShipmentNo();
                $template = 'FrontendBundle:Mail:shipment.txt.twig';
                $subject = 'Pedido Enviado';
            }

            if ($entity->getInvoiceNo() != '' || $entity->getShipmentNo() != '') {
                $message = \Swift_Message::newInstance()
                        ->setSubject($subject)
                        ->setFrom('no-reply@tanane.com')
                        ->setTo($entity->getEmail())
                        ->setContentType("text/html")
                        ->setBody($this->renderView($template, $informacion
                ));
                $this->get('mailer')->send($message);
            }
            $message2 = \Swift_Message::newInstance()
                        ->setSubject('Estatus de orden actualizado')
                        ->setFrom('no-reply@tanane.com')
                        ->setTo($entity->getEmail())
                        ->setContentType("text/html")
                        ->setBody($this->renderView('FrontendBundle:Mail:status.txt.twig', array( 'username' => $entity->getPerson()->getDescription(), 'status' => $status->getName() )
               ));
            $this->get('mailer')->send($message2);

            $response[ 'message' ] = $this->get('translator')->trans('update.success', array( '%element%' => 'orden' ));
        } else {
            $response[ 'message' ] = $this->get('translator')->trans('update.fail', array( '%element%' => 'orden' ));
            $response[ 'error' ] = $this->getFormErrors($form);
            $this->get('ladybug')->log($response[ 'error' ]);
            $response[ 'status' ] = false;
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/admin/order/delete/{id}", name="delete-order")
     * @Method({"GET", "DELETE"})
     * @Secure(roles="IS_AUTHENTICATED_FULLY, ROLE_ADMIN")
     * @Template()
     */
    public function deleteAction($id)
    {
        $response[ 'status' ] = false;
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('FrontendBundle:Orders')->find($id);
        $response = array();
        if ($entity) {
            $products = $em->getRepository("FrontendBundle:Orders")->getOrderProducts($id);
            foreach ($products as $product) {
                $orderHasProduct = $em->getRepository('FrontendBundle:OrderHasProduct')->findOneBy(array( "order" => $id, "product" => $product[ 'order_id' ] ));
                if ($orderHasProduct) {
                    $em->remove($orderHasProduct);
                    $em->flush();
                }
            }
            $em->remove($entity);
            $em->flush();
            $response[ "data" ] = 'La orden ha sido eliminada satisfactoriamente';
            $response[ 'status' ] = true;
        } else {
            $response[ "data" ] = "La orden no se encuentra";
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/admin/order/updateProduct/{id}", name="order-update-product")
     * @Method("POST")
     * @Secure(roles="IS_AUTHENTICATED_FULLY, ROLE_PROFILE_ONE, ROLE_PROFILE_TWO, ROLE_ADMIN")
     * @Template()
     */
    public function updateProductAction(Request $request, $id)
    {
        $response[ 'status' ] = false;
        $em = $this->getDoctrine()->getManager();
        $orderHasProduct = $em->getRepository('FrontendBundle:OrderHasProduct')->findOneBy(array( "order" => $id, "product" => $request->get('pk') ));
        $response = array();
        if ($orderHasProduct) {
            $orderHasProduct->setAmount($request->get('value'));
            $em->flush();
            $response[ "data" ] = 'La cantidad del producto ha sido actualizada satisfactoriamente';
            $response[ 'status' ] = true;
        } else {
            $response[ "data" ] = "La cantidad del producto no se pudo actualizar";
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/admin/order/deleteProduct/{id}/{idp}", name="order-delete-product")
     * @Method({"GET", "DELETE"})
     * @Secure(roles="IS_AUTHENTICATED_FULLY, ROLE_PROFILE_ONE, ROLE_PROFILE_TWO, ROLE_ADMIN")
     * @Template()
     */
    public function deleteProductAction($id, $idp)
    {
        $response[ 'status' ] = false;
        $em = $this->getDoctrine()->getManager();
        $orderHasProduct = $em->getRepository('FrontendBundle:OrderHasProduct')->findOneBy(array( "order" => $id, "product" => $idp ));
        $response = array();
        if ($orderHasProduct) {
            $em->remove($orderHasProduct);
            $em->flush();
            $response[ "data" ] = 'El producto ha sido eliminado satisfactoriamente';
            $response[ 'status' ] = true;
            // return $this->redirect($this->generateUrl('edit-order', array('id' => $id )));
        } else {
            $response[ "data" ] = "'El producto no se pudo eliminar";
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/admin/order/listProduct/{id}", name="list-product-order")
     * @Method("GET")
     * @Secure(roles="IS_AUTHENTICATED_FULLY, ROLE_PROFILE_ONE, ROLE_PROFILE_TWO, ROLE_ADMIN")
     * @Template()
     */
    public function listProductAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $products = $em->getRepository("FrontendBundle:Orders")->getOrderProducts($id);

        return $this->render(
                        'BackendBundle:Order:addProduct.html.twig', array( 'products' => $products, 'id' => $id )
        );
    }

    /**
     * @Route("/admin/order/addProduct/{oid}", name="add-product")
     * @Method("POST")
     * @Secure(roles="IS_AUTHENTICATED_FULLY, ROLE_PROFILE_ONE, ROLE_PROFILE_TWO, ROLE_ADMIN")
     * @Template()
     */
    public function addProductAction(Request $request, $oid)
    {
        $response[ 'status' ] = true;
        $em = $this->getDoctrine()->getManager();
        $orderHasProduct = $em->getRepository('FrontendBundle:OrderHasProduct')->findOneBy(array( "order" => $oid, "product" => $request->get('product_id') ));
        $order = $em->getRepository('FrontendBundle:Orders')->find($oid);
        $response = array();
        if ($orderHasProduct) {
            $amount = $orderHasProduct->getAmount() + $request->get('amount');
            $orderHasProduct->setAmount($amount);
            $em->flush();
            $response[ "data" ] = 'La cantidad del producto ha sido actualizada satisfactoriamente';
        } elseif ($order) {
            $product = $em->getRepository('ProductBundle:Product')->find($request->get('product_id'));
            if ($product) {
                $orderHasProduct = new OrderHasProduct();
                $orderHasProduct->setOrder($order);
                $orderHasProduct->setProduct($product);
                $orderHasProduct->setAmount($request->get('amount'));
                $em->persist($orderHasProduct);
                $em->flush();
                $response[ "data" ] = 'Producto agregado satisfactoriamente';
            } else {
                $products = $em->getRepository("FrontendBundle:Orders")->getOrderProducts($oid);

                return $this->render(
                                'BackendBundle:Order:addProduct.html.twig', array( 'products' => $products, 'id' => $oid )
                );
                $response[ 'status' ] = false;
                $response[ "data" ] = 'No se puede agregar el producto';
            }
        } else {
            $response[ 'status' ] = false;
            $response[ "data" ] = 'No se puede agregar el producto. La orden no se encuentra';
        }

        return new JsonResponse($response);
    }

    /**
     * @param  \Symfony\Component\Form\Form $form
     * @return array
     */
    private function getFormErrors(\Symfony\Component\Form\Form $form)
    {
        $errors = array();

        foreach ($form->getErrors() as $key => $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[ $child->getName() ] = $this->getFormErrors($child);
            }
        }

        return $errors;
    }
}
