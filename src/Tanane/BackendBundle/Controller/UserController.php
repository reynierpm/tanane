<?php

namespace Tanane\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Tanane\UserBundle\Entity\User;
use Tanane\UserBundle\Form\Type\UserType;

class UserController extends Controller
{
    /**
     * @Route("/admin/user/list", name="list-user")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }

    /**
     * @Route("/admin/user/get-list", name="get-list-user")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function getUsersAction()
    {
        $response = array();
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository("UserBundle:User")->findAll();

        $users = array();
        foreach ($entities as $entity) {
            $user = array();
            $user[] = $entity->isEnabled() == FALSE ? '<i class="fa fa-times-circle-o fa-2x text-danger"></i>' : '<i class="fa fa-check-circle-o text-success fa-2x"></i>';
            $user[] = $entity->getUsername();
            $user[] = $entity->getEmailCanonical();
            $user[] = $entity->getTransformedRoles();
            $user[] = $entity->getGroupNames() != NULL ? $entity->getGroupNames() : "-";
            $authorizedLinks = '<a rel="tooltip" data-placement="top" data-original-title="Modificar" class = "btn btn-primary" href="'.$this->generateUrl('edit-user', array('id' => $entity->getId()), true).'"><i class="fa fa-pencil-square-o"></i></a>';
            if ($entity->getId() !== 1 || $entity->getUsername() !== "admin") {
                $authorizedLinks .= '&nbsp;<a rel="tooltip" data-placement="top" data-original-title="Eliminar" class="btn btn-danger confirm-delete" data-id="'.$entity->getId().'" href ="#"><i class="fa fa-trash-o"></i></a>';
            }
            $user[] = $authorizedLinks;

            $users[] = $user;
        }

        $response['data'] = $users;

        return new JsonResponse($response);
    }

    /**
     * @Route("/admin/user/new", name="new-user")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function newAction()
    {
        $entity = new User();
        $form = $this->createForm(new UserType(), $entity);

        return array('entity' => $entity, 'form' => $form->createView());
    }

    /**
     * @Route("/admin/user/save", name="save-user")
     * @Method("POST")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function saveAction(Request $request)
    {
        $entity = new User();
        $form = $this->createForm(new UserType(), $entity);
        $form->handleRequest($request);
        $response = array();
        $response['status'] = true;

        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->container->get('fos_user.user_manager');
        $user = $userManager->createUser();

        if ($form->isValid()) {
            $user_values = $request->get('user');
            $user->setUsername($user_values['username']);
            $user->setEmail($user_values['email']);

            if (isset($user_values['enabled'])) {
                $user->setEnabled($user_values['enabled']);
            }

            $user->setPlainPassword($user_values['plainPassword']['first']);

            if (isset($user_values['roles'])) {
                $user->setRoles($user_values['roles']);
            }

            $userManager->updateUser($user);

            $response['message'] = $this->get('translator')->trans('create.success', array('%element%' => 'el usuario'));
        } else {
            $response['message'] = $this->get('translator')->trans('create.fail', array('%element%' => 'el usuario'));
            $response['error'] = $this->getFormErrors($form);
            $this->get('ladybug')->log($response['error']);
            $response['status'] = false;
        }

        return new JsonResponse($response);
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/admin/user/{id}/edit", name="edit-user")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UserBundle:User')->find($id);

        $form = $this->createForm(new UserType(), $user, array(
            'action' => $this->generateUrl('update-user', array('id' => $id)),
            'method' => 'POST',
        ));

        return array(
            'form' => $form->createView(),
            'user' => $user,
        );
    }

    /**
     * @Route("/admin/user/update/{id}", name="update-user")
     * @Method("POST")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function updateAction(Request $request, $id = null)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository("UserBundle:User")->find($id);

        $form = $this->createForm(new UserType(), $user);
        $form->handleRequest($request);

        $response = array();
        $response['status'] = true;

        if ($form->isValid()) {
            $userManager = $this->container->get('fos_user.user_manager');
            $userManager->updateUser($user);
            $response['message'] = $this->get('translator')->trans('update.success', array('%element%' => 'usuario'));
        } else {
            $response['message'] = $this->get('translator')->trans('update.fail', array('%element%' => 'usuario'));
            $response['error'] = $this->getFormErrors($form);
            $this->get('ladybug')->log($response['error']);
            $response['status'] = false;
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/admin/user/delete/{id}", name="delete-user")
     * @Method({"DELETE","GET"})
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('UserBundle:User')->find($id);
        $response = array();
        $response["error"] = true;

        if ($entity) {
            $em->remove($entity);
            $em->flush();
            $response['message'] = $this->get('translator')->trans('delete.success', array('%element%' => 'usuario'));
        } else {
            $response['message'] = $this->get('translator')->trans('delete.fail', array('%element%' => 'usuario'));
            $response["error"] = false;
        }

        return new JsonResponse($response);
    }

    /**
     * @param  \Symfony\Component\Form\Form $form
     * @return array
     */
    private function getFormErrors(\Symfony\Component\Form\Form $form)
    {
        $errors = array();

        foreach ($form->getErrors() as $key => $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getFormErrors($child);
            }
        }

        return $errors;
    }
}
