<?php
/**
 * Controler:   Handle forms data save
 *
 * @package     Tanane
 * @subpackage  FrontendBundle
 * @author      Reynier Perez Mira <reynierpm@gmail.com>
 * @copyright   (c) Dynamo Technology Solutions
 */

namespace Tanane\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Tanane\FrontendBundle\Entity\NaturalPerson;
use Tanane\FrontendBundle\Entity\LegalPerson;
use Tanane\FrontendBundle\Entity\Orders;
use Tanane\FrontendBundle\Form\Type\OrdersType;
use Tanane\FrontendBundle\Entity\OrderHasProduct;
use Tanane\UserBundle\Entity\User;
use Tanane\UserBundle\Form\Type\UserType;


class OrderController extends Controller
{
    /**
     * @Route("/order/{type}", name="order")
     * @Method("GET")
     */
    public function orderAction($type)
    {
        $order = new Orders();
        $orderForm = $this->createForm(new OrdersType(), $order, array('action' => $this->generateUrl('save_order'), 'register_type' => $type));
        $template = $type === "natural" ? "FrontendBundle:Natural:natural.html.twig" : "FrontendBundle:Legal:legal.html.twig";

        return $this->render($template, array('order' => $order, "orderForm" => $orderForm->createView()));
    }

    /**
     * @Route("/save_order", name="save_order")
     * @Method("POST")
     */
    public function saveAction(Request $request)
    {
        $orders = $request->get('orders');
        $sameAddress = $request->get('same_address');

        // NaturalPerson: 1 | LegalPerson: 2
        $person_type = isset($orders['person']['nat']) ? 1 : 2;
        $register_type = isset($orders['person']['nat']) ? "natural" : "legal";

        $entityOrder = new Orders();
        $formOrder = $this->createForm(new OrdersType(), $entityOrder, array('register_type' => $register_type));

        $formOrder->handleRequest($request);

        $success = false;

        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();

        $errors = "";
        $is_new = false;

        if ($formOrder->isValid()) {
            if ($person_type === 1) {
                // Set NaturalPerson entity
                $entityPerson = $em->getRepository('FrontendBundle:NaturalPerson')->findOneBy(array("ci" => $orders['person']['ci']));
                if (!$entityPerson) {
                    $entityPerson = new NaturalPerson();
                    $entityPerson->setPersonType($person_type);
                    $entityPerson->setDescription($orders['person']['nat']['description']);
                    $entityPerson->setContactPerson($orders['person']['nat']['contact_person']);
                    $entityPerson->setIdentificationType($orders['person']['identification_type']);
                    $entityPerson->setCI($orders['person']['ci']);
                    $is_new = true;
                }
            } elseif ($person_type === 2) {
                // Set LegalPerson entity
                $entityPerson = $em->getRepository('FrontendBundle:LegalPerson')->findOneBy(array("rif" => $orders['person']['rif']));
                if (!$entityPerson) {
                    $entityPerson = new LegalPerson();
                    $entityPerson->setPersonType($person_type);
                    $entityPerson->setDescription($orders['person']['leg']['description']);
                    $entityPerson->setContactPerson($orders['person']['leg']['contact_person']);
                    $entityPerson->setIdentificationType($orders['person']['identification_type']);
                    $entityPerson->setRIF($orders['person']['rif']);
                    $is_new = true;
                }
            }

            if ($is_new) {
                $em->persist($entityPerson);
                $em->flush();
            }

            if ($orders['nickname']) {
                $entityOrder->setNickname($orders['nickname']);
            }

            $phoneNumber = $this->container->get('libphonenumber.phone_number_util')->parse($orders['phone'], "VE");
            $entityOrder->setPhone($phoneNumber);

            $entityOrder->setEmail($orders['email']);
            $entityOrder->setFiscalAddress($orders['fiscal_address']);

            if ($sameAddress) {
                $entityOrder->setShippingAddress($orders['fiscal_address']);
            } else {
                $entityOrder->setShippingAddress($orders['shipping_address']);
            }

            $entityOrder->setShippingFrom($orders['shipping_from']);

            $paymentType = $em->getRepository('CommonBundle:PaymentType')->findOneBy(array("id" => $orders['payment_type']));
            $entityOrder->setPaymentType($paymentType);

            $entityOrder->setOrderAmount($orders['order_amount']);

            $bank = $em->getRepository('CommonBundle:Bank')->findOneBy(array("id" => $orders['bank']));
            $entityOrder->setBank($bank);

            $entityOrder->setTransaction($orders['transaction']);
            $entityOrder->setComments($orders['comments']);
            $entityOrder->setPerson($entityPerson);

            $status = $em->getRepository('CommonBundle:OrderStatus')->find(1);
            $entityOrder->setStatus($status);

            $em->persist($entityOrder);
            $em->flush();

            // products save
            $product_request = $request->request->get('ProductoForm');
            $products = array();
            $valid = true;

            foreach ($product_request as $product_r) {
                if (intval($product_r['amount']) > 0) {
                    if (!array_key_exists($product_r['product_name'], $products)) {
                        $products[$product_r['product_name']] = (object) $product_r;
                    } else {
                        $products[$product_r['product_name']]->amount += $product_r['amount'];
                    }
                }
            }
            $product_list = array();
            foreach ($products as $product) {
                $productEntity = $em->getRepository('ProductBundle:Product')->findOneBy(array("product_name" => $product->product_name));
                if ($productEntity) {
                    $product_list[] = $product->product_name;
                    $OrderHasProductEntity = new OrderHasProduct();
                    $OrderHasProductEntity->setOrder($entityOrder);
                    $OrderHasProductEntity->setProduct($productEntity);
                    $OrderHasProductEntity->setAmount($product->amount);

                    $em->persist($OrderHasProductEntity);
                    $em->flush();
                } else {
                    $valid = false;
                }
            }

            if (!$valid) {
                $em->getConnection()->rollback();
                $success = false;
            } else {
                $response['message'] = 'DATOS PROCESADOS CON EXITO';
                $em->getConnection()->commit();
                $success = true;
                // Suscribe users to mailing list
                if (isset($orders['suscribe_mail_list'])) {
                    $mailchimp = $this->get('hype_mailchimp');

                    $data = $mailchimp
                            ->getList()
                            ->addMerge_vars(
                                    array(
                                        'FCNAME' => $person_type === 1 ? $orders['nat']['person']['description'] : $orders['leg']['person']['description'],
                            ))
                            ->subscribe($orders['email']);
                }
                $person = $entityOrder->getPerson();

                $users = $em->getRepository("UserBundle:User")->findAll();
                $to = $entityOrder->getEmail();
                $bcc = array();
                foreach ($users as $user) {
                    if($user->getTransformedRoles()[0] == 'Administrador')
                        $bcc[] = $user->getEmailCanonical();
                }
                //Send email to user
                $message = \Swift_Message::newInstance()
                        ->setSubject('Datos de la Orden')
                        ->setFrom('no-reply@tanane.com')
                        ->setTo($to)
                        ->setBcc($bcc)
                        ->setContentType("text/html");

                $informacion = array(
                    'fecha' => date("F j, Y, g:i a"),
                    'nickname' => $entityOrder->getNickname(),
                    'nombre' => $person->getDescription(),
                    'cedula' => ($person_type === 1) ? $person->getIdentificationType().$person->getCI() : $person->getIdentificationType().$person->getRIF(),
                    'telefono' => $this->container->get('libphonenumber.phone_number_util')->format($entityOrder->getPhone(), 'NATIONAL'),
                    'correo' => $entityOrder->getEmail(),
                    'direccion' => $entityOrder->getFiscalAddress(),
                    'direccion_envio' => $entityOrder->getShippingAddress(),
                    'enviado' => $entityOrder->getShippingFrom(),
                    'productos' => implode(", <br/>", $product_list),
                    'pago' => $entityOrder->getPaymentType(),
                    'monto' => $entityOrder->getOrderAmount(),
                    'banco' => $entityOrder->getBank(),
                    'transferencia' => $entityOrder->getTransaction(),
                );

                $message->setBody($this->renderView('FrontendBundle:Mail:process.txt.twig', $informacion), 'text/html');
                $this->get('mailer')->send($message);
            }
        } else {
            $success = false;
            $em->getConnection()->rollback();
        }

        $url = $this->generateUrl('process');

        return new JsonResponse(array(
            'success' => $success,
            'errors' => $errors,
            'redirect_to' => $url, )
        );
    }

    private function getFormErrors(\Symfony\Component\Form\Form $form)
    {
        $errors = array();

        foreach ($form->getErrors() as $key => $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getFormErrors($child);
            }
        }

        return $errors;
    }
}
