<?php

namespace Tanane\FrontendBundle\Model;

use Mremi\ContactBundle\Model\Contact;

class TananeContact extends Contact
{
    /**
     * @var string
     */
    protected $company;

    /**
     * @var string
     */
    protected $phone;

    public function __construct()
    {
        parent::__construct();
    }

    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * {@inheritdoc}
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * {@inheritdoc}
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * {@inheritdoc}
     */
    public function getPhone()
    {
        return $this->phone;
    }

    public function setTitle($title)
    {
        if ($title) {
            parent::setTitle($title);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function toArray()
    {
        return array(
            'title' => $this->title,
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
            'email' => $this->email,
            'subject' => $this->subject,
            'message' => $this->message,
            'company' => $this->company,
            'phone' => $this->phone,
            'createdAt' => $this->createdAt->format('c'),
        );
    }
}
