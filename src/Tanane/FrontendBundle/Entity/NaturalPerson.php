<?php
/**
 * Entidad:     Persona Natural
 *
 * @package     Tanane
 * @subpackage  ProductBundle
 * @author      Reynier Perez Mira <reynierpm@gmail.com>
 * @copyright   (c) Dynamo Technology Solutions
 */

namespace Tanane\FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Tanane\FrontendBundle\DBAL\Types\CIType;
use Fresh\Bundle\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="natural_person")
 * @UniqueEntity(fields={"ci"}, message="Este CI ya existe en nuestra base de datos")
 */
class NaturalPerson extends Person
{
    /**
     * @ORM\Column(name="identification_type", type="ci_type", nullable=false)
     * @DoctrineAssert\Enum(entity="Tanane\FrontendBundle\DBAL\Types\CIType")
     */
    protected $identification_type;

    /**
     * @ORM\Column(name="ci", type="integer", nullable=false)
     */
    protected $ci;

    public function setIdentificationType($identification_type)
    {
        $this->identification_type = $identification_type;

        return $this;
    }

    public function getIdentificationType()
    {
        return $this->identification_type;
    }

    public function setCI($ci)
    {
        $this->ci = $ci;

        return $this;
    }

    public function getCI()
    {
        return $this->ci;
    }
}
