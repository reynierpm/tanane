<?php
/**
 * Entidad:     Persona Juridica
 *
 * @package     Tanane
 * @subpackage  ProductBundle
 * @author      Reynier Perez Mira <reynierpm@gmail.com>
 * @copyright   (c) Dynamo Technology Solutions
 */

namespace Tanane\FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Tanane\FrontendBundle\DBAL\Types\RifType;
use Fresh\Bundle\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="legal_person")
 * @UniqueEntity(fields={"rif"}, message="Este RIF ya existe en nuestra base de datos")
 */
class LegalPerson extends Person
{
    /**
     * @ORM\Column(name="identification_type", type="rif_type", nullable=false)
     * @DoctrineAssert\Enum(entity="Tanane\FrontendBundle\DBAL\Types\RifType")
     */
    protected $identification_type;

    /**
     * @ORM\Column(name="rif", type="integer", nullable=false)
     */
    protected $rif;

    public function setIdentificationType($identification_type)
    {
        $this->identification_type = $identification_type;

        return $this;
    }

    public function getIdentificationType()
    {
        return $this->identification_type;
    }

    public function setRIF($rif)
    {
        $this->rif = $rif;

        return $this;
    }

    public function getRIF()
    {
        return $this->rif;
    }
}
