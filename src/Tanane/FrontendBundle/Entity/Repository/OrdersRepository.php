<?php
/**
 * Repositorio: Orders
 *
 * @package     Tanane
 * @subpackage  FrontendBundle
 * @author      Reynier Perez Mira <reynierpm@gmail.com>
 * @copyright   (c) Dynamo Technology Solutions
 */

namespace Tanane\FrontendBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class OrdersRepository extends EntityRepository
{
    public function findAll()
    {
        return $this->findBy(array(), array('id' => 'DESC', 'createdAt' => 'DESC'));
    }

    /**
     * Get orders related info
     * @param  integer $person_type The type of person default to NaturalPerson
     * @return type
     */
    public function getOrders($person_type = 1)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb
                ->select('ord.*, ps.*')
                ->from("FrontendBundle:Orders", "ord")
                ->join('FrontendBundle:Person', 'ps', 'WITH', 'ps.id = ord.person_id')
                ->orderBy('ord.created', 'DESC');

        if ($person_type == 1) {
            $qb
                    ->select('np.*')
                    ->join('FrontendBundle:NaturalPerson', 'np', 'WITH', 'ps.id = np.person'); // Join NaturalPerson table
        } elseif ($person_type == 2) {
            $qb
                    ->select('lp.*')
                    ->join('FrontendBundle:LegalPerson', 'lp', 'WITH', 'ps.id = lp.person'); // Join NaturalPerson table
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * Get order latest status
     * @param  integer $order_id The order_id to look for
     * @return type
     */
    public function getOrderLatestSatus($order_id)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb
                ->select('os.name')
                ->from("FrontendBundle:Orders", "ord")
                ->where('ord.id = :order_id')
                ->join('FrontendBundle:OrderHasStatus', 'ohs', 'WITH', 'ohs.order = ord.id')
                ->join('CommonBundle:OrderStatus', 'os', 'WITH', 'ohs.status = os.id')
                ->orderBy('ohs.createdAt', 'DESC')
                ->andWhere('os.active = :status')
                ->setParameter('order_id', $order_id)
                ->setParameter('status', 1);

        return $qb->getQuery()->getResult();
    }

    /**
     * Get order products
     * @param  integer $order_id The order_id to look for
     * @return type
     */
    public function getOrderProducts($order_id)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb
                ->select('ord.id order_id, op.id as product_id, op.product_name, ohp.amount')
                ->from("FrontendBundle:Orders", "ord")
                ->where('ord.id = :order_id')
                ->join('FrontendBundle:OrderHasProduct', 'ohp', 'WITH', 'ohp.order = ord.id')
                ->join('ProductBundle:Product', 'op', 'WITH', 'ohp.product = op.id')
                ->orderBy('ord.createdAt', 'DESC')
                ->setParameter('order_id', $order_id);

        return $qb->getQuery()->getResult();
    }
}
