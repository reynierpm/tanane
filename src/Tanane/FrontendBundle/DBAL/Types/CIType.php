<?php
/**
 * Tipo:        Tipo para CI (ENUM)
 *
 * @package     Tanane
 * @subpackage  FrontendBundle
 * @author      Reynier Perez Mira <reynierpm@gmail.com>
 * @copyright   (c) Dynamo Technology Solutions
 */

namespace Tanane\FrontendBundle\DBAL\Types;

use Fresh\Bundle\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class CIType extends AbstractEnumType
{
    const V = "V";
    const E = "E";
    const P = "P";

    /**
     * @var string Name of this type
     */
    protected $name = 'ci_type';

    /**
     * @var array Readable choices
     * @static
     */
    protected static $choices = [
        self::V => 'V',
        self::E => 'E',
        self::P => 'P',
    ];
}
