<?php
/**
 * Tipo:        Tipo para Metodo de envio (ENUM)
 *
 * @package     Tanane
 * @subpackage  FrontendBundle
 * @author      Reynier Perez Mira <reynierpm@gmail.com>
 * @copyright   (c) Dynamo Technology Solutions
 */

namespace Tanane\FrontendBundle\DBAL\Types;

use Fresh\Bundle\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class SFType extends AbstractEnumType
{
    const DOMESA = "DOMESA";

    const ZOOM = "ZOOM";

    const MRW = "MRW";

    /**
     * @var string Name of this type
     */
    protected $name = 'shipping_from_type';

    /**
     * @var array Readable choices
     * @static
     */
    protected static $choices = [
        self::MRW => 'MRW - COBRO EN DESTINO',
        self::DOMESA => 'DOMESA - COBRO EN DESTINO',
        self::ZOOM => 'GRUPO ZOOM - COBRO EN DESTINO',
    ];
}
