<?php
/**
 * Tipo:        Tipo para RIF (ENUM)
 *
 * @package     Tanane
 * @subpackage  FrontendBundle
 * @author      Reynier Perez Mira <reynierpm@gmail.com>
 * @copyright   (c) Dynamo Technology Solutions
 */

namespace Tanane\FrontendBundle\DBAL\Types;

use Fresh\Bundle\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class RifType extends AbstractEnumType
{
    const J = "J";
    const G = "G";
    const V = "V";

    /**
     * @var string Name of this type
     */
    protected $name = 'rif_type';

    /**
     * @var array Readable choices
     * @static
     */
    protected static $choices = [
        self::J => 'J',
        self::G => 'G',
        self::V => 'V',
    ];
}
