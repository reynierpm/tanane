<?php

namespace Tanane\FrontendBundle\Form\Type;

use Mremi\ContactBundle\Form\Type\ContactType;
use Symfony\Component\Form\FormBuilderInterface;

class TananeContactType extends ContactType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
                ->remove('title')
                ->add('company', 'text', array('label' => 'Compañía'))
                ->add('phone', 'text', array('label' => 'Teléfono(s)'));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'tanane_contact';
    }
}
