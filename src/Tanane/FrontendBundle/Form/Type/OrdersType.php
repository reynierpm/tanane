<?php

namespace Tanane\FrontendBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Tanane\FrontendBundle\DBAL\Types\SFType;
use libphonenumber\PhoneNumberFormat;
use Doctrine\ORM\EntityRepository;

class OrdersType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('nickname', 'text', array(
                    'required' => FALSE,
                    'label' => "Nickname/Seudónimo",
                    'trim' => TRUE,
                    'attr' => array(
                        'class' => 'nickname',
                    ),
                ))
                ->add('email', 'email', array(
                    'required' => TRUE,
                    'label' => "Correo Electrónico",
                    'trim' => TRUE,
                ))
                ->add('phone', 'tel', array(
                    'required' => TRUE,
                    'label' => 'Números de teléfono (separados por "/")',
                    'trim' => TRUE,
                    'default_region' => 'VE',
                    'format' => PhoneNumberFormat::NATIONAL,
                ))
                ->add('fiscal_address', 'textarea', array(
                    'required' => TRUE,
                    'label' => 'Dirección',
                ))
                ->add('shipping_address', 'textarea', array(
                    'required' => TRUE,
                    'label' => 'Dirección de Envío',
                ))
                ->add('shipping_from', 'choice', array(
                    'label' => 'Compañía de Encomiendas',
                    'choices' => SFType::getChoices(),
                    'empty_value' => '-- SELECCIONAR --',
                ))
                ->add('payment_type', 'entity', array(
                    'class' => 'CommonBundle:PaymentType',
                    'property' => 'name',
                    'required' => TRUE,
                    'label' => 'Forma de Pago',
                    'empty_value' => '-- SELECCIONAR --',
                ))
                ->add('order_amount', 'number', array(
                    'label' => 'Monto',
                    'required' => TRUE,
                    'precision' => 2,
                ))
                ->add('bank', 'entity', array(
                    'class' => 'CommonBundle:Bank',
                    'property' => 'name',
                    'required' => TRUE,
                    'label' => 'Banco',
                    'empty_value' => '-- SELECCIONAR --',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('b')
                                ->where('b.active = :activeValue')
                                ->setParameter('activeValue', true);
                    },
                ))
                ->add('transaction', 'text', array(
                    'required' => TRUE,
                    'label' => 'No. Transacción',
                ))
                ->add('comments', 'textarea', array(
                    'required' => FALSE,
                    'label' => 'Comentarios',
                ))
                ->add('secure', 'checkbox', array(
                    'label' => FALSE,
                    'required' => FALSE,
                    'value' => 1,
                    'mapped' => FALSE,
                ))
                ->add('lives_in_ccs', 'checkbox', array(
                    'label' => false,
                    'required' => false,
                    'mapped' => FALSE,
                    'value' => 1,
                ))
                ->add('suscribe_mail_list', 'checkbox', array(
                    'label' => FALSE,
                    'required' => FALSE,
                    'value' => 1,
                    'mapped' => FALSE,
        ));

        if ($options[ 'curr_action' ] !== NULL) {
            $builder
                    ->add('status', 'entity', array(
                        'class' => 'CommonBundle:OrderStatus',
                        'property' => 'name',
                        'required' => TRUE,
                        'label' => FALSE,
                    ))
                    ->add('invoice_no', 'text', array(
                        'required' => TRUE,
                        'label' => FALSE,
                        'trim' => TRUE,
                    ))
                    ->add('shipment_no', 'text', array(
                        'required' => TRUE,
                        'label' => FALSE,
                        'trim' => TRUE,
            ));
        }

        if ($options[ 'register_type' ] == "natural" || $options[ 'register_type' ] == 1) {
            $builder->add('person', new NaturalPersonType(), array( 'label' => FALSE ));
        } elseif ($options[ 'register_type' ] == "legal" || $options[ 'register_type' ] === 2) {
            $builder->add('person', new LegalPersonType(), array( 'label' => FALSE ));
        }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setRequired(array(
            'register_type',
        ));

        $resolver->setOptional(array(
            'curr_action',
        ));

        $resolver->setDefaults(array(
            'data_class' => 'Tanane\FrontendBundle\Entity\Orders',
            'intention' => 'orders_form',
            'curr_action' => NULL,
        ));
    }

    public function getName()
    {
        return 'orders';
    }
}
