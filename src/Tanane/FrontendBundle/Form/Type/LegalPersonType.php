<?php

namespace Tanane\FrontendBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Tanane\FrontendBundle\DBAL\Types\RifType;

class LegalPersonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('identification_type', 'choice', array(
                    'label' => FALSE,
                    'choices' => RifType::getChoices(),
                ))
                ->add('rif', 'number', array(
                    'required' => true,
                    'label' => false,
                    'attr' => array(
                        'maxlength' => 10,
                    ), )
                )
                ->add('leg', new PersonType(), array(
                    'label' => FALSE,
                    'data_class' => 'Tanane\FrontendBundle\Entity\LegalPerson',
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Tanane\FrontendBundle\Entity\LegalPerson',
        ));
    }

    public function getName()
    {
        return 'legal_person';
    }
}
