<?php

namespace Tanane\FrontendBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PersonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('description', 'text', array(
                    'required' => TRUE,
                    'label' => FALSE,
                ))
                ->add('contact_person', 'text', array(
                    'required' => FALSE,
                    'label' => 'Persona de Contacto',
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'inherit_data' => true,
        ));
    }

    public function getName()
    {
        return 'person';
    }
}
