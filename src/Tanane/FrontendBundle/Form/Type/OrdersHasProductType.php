<?php

namespace Tanane\FrontendBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class OrdersHasProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('product', 'text', array(
                    'required' => FALSE,
                    'label' => FALSE,
                ))
                ->add('amount', 'text', array(
                    'required' => TRUE,
                    'label' => FALSE,
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Tanane\FrontendBundle\Entity\OrderHasProduct',
            'intention' => 'order_has_product',
        ));
    }

    public function getName()
    {
        return 'order_has_product';
    }
}
