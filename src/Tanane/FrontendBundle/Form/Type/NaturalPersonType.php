<?php

namespace Tanane\FrontendBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Tanane\FrontendBundle\DBAL\Types\CIType;

class NaturalPersonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('identification_type', 'choice', array(
                    'label' => FALSE,
                    'choices' => CIType::getChoices(),
                ))
                ->add('ci', 'number', array(
                    'required' => true,
                    'label' => false,
                    'attr' => array(
                        'maxlength' => 8,
                    ), )
                )
                ->add('nat', new PersonType(), array(
                    'label' => FALSE,
                    'data_class' => 'Tanane\FrontendBundle\Entity\NaturalPerson',
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Tanane\FrontendBundle\Entity\NaturalPerson',
        ));
    }

    public function getName()
    {
        return 'natural_person';
    }
}
