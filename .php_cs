<?php

return Symfony\CS\Config\Config::create()->finder(Symfony\CS\Finder\DefaultFinder::create()
    ->exclude('Symfony/CS/Tests/Fixtures')
    ->in(__DIR__)
);